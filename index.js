// console.log("hello");

// ARITHMETIC OPERATORS

let x = 1397;
let y = 7831;

//let sum = 1397 + 7831
let	sum = x + y;
console.log("Result of addtion operator: " + sum);

let difference = x-y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

//Modulus (%)
//gets the remainder from 2 divided values
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// ASSIGNMENT OPERATOR (=)
let assignmentNumer = 8;

assignmentNumer = assignmentNumer + 2;
console.log("Result of addtion assignment operator: " + assignmentNumer);

//shorthand method for assignment operator
assignmentNumer += 2
console.log("Result of addtion assignment operator: " + assignmentNumer);

assignmentNumer -= 2
console.log("Result of subtraction assignment operator: " + assignmentNumer);

assignmentNumer *= 2
console.log("Result of multiplication assignment operator: " + assignmentNumer);

assignmentNumer /= 2
console.log("Result of division assignment operator: " + assignmentNumer);

//Multiple operators and parenthesis

let mdas = 1+2-3*4/5
console.log("Result of mdas operation: " + mdas); 

let pemdas = 1 + (2-3) * (4/5);
console.log("Result of pemdas operation: " + pemdas);

pemdas = (1 + (2-3)) * (4 / 5);
console.log("Result of second pemdas operation: " + pemdas);

//Incrementation vs Decrementation
//Incrementation (++)
//Decrementation (--)
//pre-incrementation ++z added 1 to its original value.
let z = 1;
let increment = ++z;
console.log("Result of pre-incrementation: " + increment);
console.log("Result of pre-incrementation: " + z);

//post-incrementation z++;
increment = z++;
console.log("Result of post-incrementation: " + increment)
console.log("Result of post-incrementation: " + z);


//pre-incrementation --z;
let decrement = --z;
console.log("Result of pre-decrementation: " + decrement);
console.log("Result of pre-decrementation: " + z);

//post-incrementation z--;
decrement = z--;
console.log("Result of post-incrementation: " + decrement)
console.log("Result of post-incrementation: " + z);

//type coercio
let numA="10";
let numB= 12;

let coercion = numA + numB
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC = numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

//false = 0 default
let numE = false + 1;
console.log(numE);

//true = 1 default
let numF = true + 1;
console.log(numF);

//Comparison operators

let	 juan = "juan";

//Equality operator (==)
// Check 2 Operans if they are equal/have the same content
//May return boolean value

console.log(1==1);
console.log(1==2);
console.log(1=="1");
console.log("juan"=="juan");
console.log("juan"== juan);

//Inequality Operator (!=)
console.log(1!=1);
console.log(1!=2);
console.log(1!="1");
console.log("juan"!="juan");
console.log("juan"!= juan);
console.log(0 != false);

// Strict Equality Operator (===)
// Compares the content and also the data type
console.log(1 == 1);
console.log(1 === 2);
console.log("juan" === juan);

// Strict Inequality Operator (===)
// Compares the content and also the data type
console.log(1 !== 1);
console.log(1 !== 2);
console.log("juan" !== juan);

//Relational Operator
let a = 50;
let b = 65;

// GT (>) Greater than operator
let isGreaterThan = a > b;
console.log(isGreaterThan);

// LT (<) lessthan operator
let isLessThan = a < b;
console.log(isLessThan);

// GTE (>=)
let isGTorEqual = a >= b;
console.log(isGTorEqual);

//LTE(<=)
let isLTorEqual = a <= b;
console.log(isLTorEqual);
////////////////////////////////////////
let numStr = "30";
console.log(a > numStr); //kung sino 
console.log(numStr > a);

let str = "twenty";
console.log (b >= str);

//In some events, we can receive NaN
//NaN == Not a Number

//Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&& - Ampersands)
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " + allRequirementsMet);

// Logical OR Operator ( || - Double Pipe)
let someRequirementsMet = isLegalAge ||  isRegistered;
console.log("Result of logical OR Operator: " + someRequirementsMet);

// Logical NOT Operator (! = Exclamation Poit)
let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT Operator: " + someRequirementsNotMet)